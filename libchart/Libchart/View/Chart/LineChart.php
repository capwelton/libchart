<?php
    /* Libchart - PHP chart library
     * Copyright (C) 2005-2011 Jean-Marc Tr�meaux (jm.tremeaux at gmail.com)
     *
     * This program is free software: you can redistribute it and/or modify
     * it under the terms of the GNU General Public License as published by
     * the Free Software Foundation, either version 3 of the License, or
     * (at your option) any later version.
     *
     * This program is distributed in the hope that it will be useful,
     * but WITHOUT ANY WARRANTY; without even the implied warranty of
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     * GNU General Public License for more details.
     *
     * You should have received a copy of the GNU General Public License
     * along with this program.  If not, see <http://www.gnu.org/licenses/>.
     *
     */

    namespace Libchart\View\Chart;

    /**
     * Line chart.
     *
     * @author Jean-Marc Tr�meaux (jm.tremeaux at gmail.com)
     */
    class LineChart extends \Libchart\View\Chart\BarChart {

//        public $lineThickness = 8;

        public $filled = false;


        /**
         * Creates a new line chart.
         * Line charts allow for XYDataSet and XYSeriesDataSet in order to plot several lines.
         *
         * @param integer width of the image
         * @param integer height of the image
         */
        public function __construct($width = 600, $height = 250)
        {
            parent::__construct($width, $height);

            $this->plot->setGraphPadding(new \Libchart\View\Primitive\Padding(5, 30, 100, 100));
        }

        /**
         * Computes the layout.
         */
        protected function computeLayout()
        {
            if ($this->hasSeveralSerie) {
                $this->plot->setHasCaption(true);
            }
            $this->plot->computeLayout();
        }

        /**
         * Print the axis.
         */
        protected function printAxis()
        {
            $minValue = $this->axis->getLowerBoundary();
            $maxValue = $this->axis->getUpperBoundary();
            $stepValue = $this->axis->getTics();

            // Get graphical obects
            $img = $this->plot->getImg();
            $palette = $this->plot->getPalette();
            $text = $this->plot->getText();
            $primitive = $this->plot->getPrimitive();

            // Get the graph area
            $graphArea = $this->plot->getGraphArea();

            $labelGenerator = $this->plot->getLabelGenerator();

            // Vertical axis
            imagerectangle($img, $graphArea->x1 - 1, $graphArea->y1, $graphArea->x1, $graphArea->y2, $palette->axisColor[0]->getColor($img));

            for ($value = $minValue; $value <= $maxValue; $value += $stepValue) {
                $y = $graphArea->y2 - ($value - $minValue) * ($graphArea->y2 - $graphArea->y1) / ($this->axis->displayDelta);

                $primitive->line($graphArea->x1 - 3, $y, $graphArea->x2, $y, $palette->axisColor[0], 0.5);

                $label = $labelGenerator->generateLabel($value);
                $text->printText($img, $graphArea->x1 - 5, $y, $this->plot->getTextColor(), $label, $text->fontCondensed, $text->HORIZONTAL_RIGHT_ALIGN | $text->VERTICAL_CENTER_ALIGN);
            }

            // Get first serie of a list
            $pointList = $this->getFirstSerieOfList();

            // Horizontal Axis
            $pointCount = count($pointList);
            reset($pointList);
            $columnWidth = ($graphArea->x2 - $graphArea->x1) / ($pointCount - 1);
            $horizOriginY = $graphArea->y2 + $minValue * ($graphArea->y2 - $graphArea->y1) / ($this->axis->displayDelta);

            imagerectangle($img, $graphArea->x1 - 1, $horizOriginY, $graphArea->x2, $horizOriginY + 1, $palette->axisColor[0]->getColor($img));

            for ($i = 0; $i < $pointCount; $i++) {
                $x = $graphArea->x1 + $i * $columnWidth;

                $primitive->line($x, $graphArea->y2 + 2, $x, $graphArea->y1, $palette->axisColor[0], 0.5);

                $point = current($pointList);
                next($pointList);

                $label = $point->getX();

                $text->printDiagonal($img, $x - 5, $graphArea->y2 + 10, $this->plot->getTextColor(), $label);
            }
        }

        /**
         * Print the lines.
         */
        protected function printLine()
        {
            $minValue = $this->axis->getLowerBoundary();
            $maxValue = $this->axis->getUpperBoundary();

            // Get the data as a list of series for consistency
            $serieList = $this->getDataAsSerieList();

            // Get graphical obects
            $img = $this->plot->getImg();
            $palette = $this->plot->getPalette();
            $text = $this->plot->getText();
            $primitive = $this->plot->getPrimitive();

            // Get the graph area
            $graphArea = $this->plot->getGraphArea();

            $lineColorSet = $palette->lineColorSet;
            $lineColorSet->reset();

            $y0 = $graphArea->y2 - (0 - $minValue) * ($graphArea->y2 - $graphArea->y1) / ($this->axis->displayDelta);

            for ($j = 0; $j < count($serieList); $j++) {
                $serie = $serieList[$j];

                $lineDash = $serie->getBorderDash();
                $lineWidth = $serie->getBorderWidth();
                $pointList = $serie->getPointList();

                $pointCount = count($pointList);
                reset($pointList);

                $columnWidth = ($graphArea->x2 - $graphArea->x1) / ($pointCount - 1);

                $lineColor = $lineColorSet->currentColor();
                $fillColor = $lineColor->applyAlpha(230);

                $lineColorShadow = $lineColorSet->currentShadowColor();
                $lineColorSet->next();
                $x1 = null;
                $y1 = null;
                $previousValue = null;


                $points = array($graphArea->x1, $y0);

                for ($i = 0; $i < $pointCount; $i++) {
                    $x2 = $graphArea->x1 + $i * $columnWidth;

                    $point = current($pointList);
                    next($pointList);

                    $value = $point->getY();

                    $y2 = $graphArea->y2 - ($value - $minValue) * ($graphArea->y2 - $graphArea->y1) / ($this->axis->displayDelta);

                    $points[] = $x2;
                    $points[] = $y2;

                    // Draw line
                    if ($x1 && $value && $previousValue) {
                        $primitive->line($x1, $y1, $x2, $y2, $lineColor, $lineWidth, $lineDash);
                    }

                    $previousValue = $value;
                    $x1 = $x2;
                    $y1 = $y2;
                }

                $points[] = $graphArea->x2;
                $points[] = $y0;

                if ($this->filled) {
                    $primitive->filledPolygon($points, $fillColor);
                }
            }
        }

        /**
         * Renders the caption.
         */
        protected function printCaption()
        {
            // Get the list of labels
            $labelList = $this->dataSet->getTitleList();

            // Create the caption
            $caption = new \Libchart\View\Caption\Caption();
            $caption->setPlot($this->plot);
            $caption->setLabelList($labelList);

            $palette = $this->plot->getPalette();
            $lineColorSet = $palette->lineColorSet;
            $caption->setColorSet($lineColorSet);

            // Render the caption
            $caption->render();
        }

        /**
         * Render the chart image.
         *
         * @param string name of the file to render the image to (optional)
         */
        public function render($fileName = null)
        {
            // Check the data model
            $this->checkDataModel();

            $this->bound->computeBound($this->dataSet);
            $this->computeAxis();
            $this->computeLayout();
            $this->createImage();
            $this->plot->printLogo();
            $this->plot->printTitle();
            if (!$this->isEmptyDataSet(2)) {
                $this->printAxis();
                $this->printLine();
                if ($this->hasSeveralSerie) {
                    $this->printCaption();
                }
            }

            $this->plot->render($fileName);
        }
    }
?>