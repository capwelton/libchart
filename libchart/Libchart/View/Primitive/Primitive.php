<?php
    /* Libchart - PHP chart library
     * Copyright (C) 2005-2011 Jean-Marc Tr�meaux (jm.tremeaux at gmail.com)
     *
     * This program is free software: you can redistribute it and/or modify
     * it under the terms of the GNU General Public License as published by
     * the Free Software Foundation, either version 3 of the License, or
     * (at your option) any later version.
     *
     * This program is distributed in the hope that it will be useful,
     * but WITHOUT ANY WARRANTY; without even the implied warranty of
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     * GNU General Public License for more details.
     *
     * You should have received a copy of the GNU General Public License
     * along with this program.  If not, see <http://www.gnu.org/licenses/>.
     *
     */

    namespace Libchart\View\Primitive;

    /**
     * Graphic primitives, extends GD with chart related primitives.
     *
     * @author Jean-Marc Tr�meaux (jm.tremeaux at gmail.com)
     */
    class Primitive {
        private $img;

        /**
         * Creates a new primitive object
         *
         * @param    resource    GD image resource
         */
        public function __construct($img) {
            $this->img = $img;
        }


        /**
         * @param \Libchart\View\Color\Color    $color  line color
         * @param int                           $lineWidth
         * @param int[]                         $lineDash
         * @return string[]
         */
        public function getDottedStyle($color, $lineWidth, $lineDash)
        {
            $colors = array(
                0 => $color->getColor($this->img),
                1 => IMG_COLOR_TRANSPARENT
            );

            $style = array();
            $colorIndex = 0;
            foreach ($lineDash as $dashLength) {
                $currentColor = $colors[$colorIndex];
                $colorIndex = ($colorIndex + 1) % 2;
                for ($i = 0; $i < $lineWidth * $lineWidth * $dashLength; $i++) {
                    $style[] = $currentColor;
                }
            }

            return $style;
        }


        /**
         * Draws a straight line.
         *
         * @param int                           $x1 line start (X)
         * @param int                           $y1 line start (Y)
         * @param int                           $x2 line end (X)
         * @param int                           $y2 line end (Y)
         * @param \Libchart\View\Color\Color    $color      The line color
         * @param float                         $lineWidth  The line width in pixels
         * @param int[]|null                    $lineDash   The line pattern
         */
        public function line($x1, $y1, $x2, $y2, $color, $lineWidth = 1, $lineDash = null)
        {
            imagesetthickness($this->img, $lineWidth);

            if ($lineDash) {
                $style = $this->getDottedStyle($color, $lineWidth, $lineDash);
                imagesetstyle($this->img, $style);
                imageline($this->img, $x1, $y1, $x2, $y2, IMG_COLOR_STYLED);
            } else {
                imageline($this->img, $x1, $y1, $x2, $y2, $color->getColor($this->img));
            }
        }


        /**
         * Draws a polygon.
         *
         * @param int[]                         $points     array(x0, y0, x1, y1...)
         * @param \Libchart\View\Color\Color    $color      The line color
         * @param float                         $lineWidth  The line width in pixels
         * @param int[]|null                    $lineDash   The line pattern
         */
        public function openPolygon($points, $color, $lineWidth = 1, $lineDash = null)
        {
            imagesetthickness($this->img, $lineWidth);

            if ($lineDash) {
                $style = $this->getDottedStyle($color, $lineWidth, $lineDash);
                imagesetstyle($this->img, $style);
                imageopenpolygon($this->img, $points, count($points) / 2, IMG_COLOR_STYLED);
            } else {
                imageopenpolygon($this->img, $points, count($points) / 2, $color->getColor($this->img));
            }
        }

        /**
         * Draws a filled polygon.
         *
         * @param int[]                         $points     array(x0, y0, x1, y1...)
         * @param \Libchart\View\Color\Color    $fillColor  The color used to fill the polygon
         */
        public function filledPolygon($points, $fillColor)
        {
            imagefilledpolygon($this->img, $points, count($points) / 2, $fillColor->getColor($this->img));
        }

        /**
         * Draw a filled gray box with thick borders and darker corners.
         *
         * @param integer top left coordinate (x)
         * @param integer top left coordinate (y)
         * @param integer bottom right coordinate (x)
         * @param integer bottom right coordinate (y)
         * @param \Libchart\View\Color\Color edge color
         * @param \Libchart\View\Color\Color corner color
         */
        public function outlinedBox($x1, $y1, $x2, $y2, $color0, $color1)
        {
            imagefilledrectangle($this->img, $x1, $y1, $x2, $y2, $color0->getColor($this->img));
            imagerectangle($this->img, $x1, $y1, $x1 + 1, $y1 + 1, $color1->getColor($this->img));
            imagerectangle($this->img, $x2 - 1, $y1, $x2, $y1 + 1, $color1->getColor($this->img));
            imagerectangle($this->img, $x1, $y2 - 1, $x1 + 1, $y2, $color1->getColor($this->img));
            imagerectangle($this->img, $x2 - 1, $y2 - 1, $x2, $y2, $color1->getColor($this->img));
        }

    }
?>